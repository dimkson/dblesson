CREATE TABLE clients
(
	id serial PRIMARY KEY,
	first_name varchar(64) NOT NULL,
	last_name varchar(64) NOT NULL,
	middle_name varchar(64),
	birth_date date NOT NULL,
	passport bigint NOT NULL
);

CREATE TABLE accounts
(
	id serial PRIMARY KEY,
	card_number bigint NOT NULL,
	balance decimal NOT NULL,
	client_id int REFERENCES clients(id)
);

CREATE TABLE operations_type
(
	id serial PRIMARY KEY,
	operation_type varchar(32) NOT NULL
);

CREATE TABLE operations
(
	id serial PRIMARY KEY,
	operation_type_id int REFERENCES operations_type(id),
	value decimal NOT NULL,
	account_id int REFERENCES accounts(id)
);