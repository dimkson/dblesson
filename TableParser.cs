﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DBLesson
{
    class TableParser
    {
        private string _tableName;
        private DataTable _dataTable;
        private NpgsqlParameter[] _parameters;
        private string _insertComText;

        public NpgsqlParameter[] Parameters
        {
            get { return _parameters; }
        }
        public string InsertComText
        {
            get { return _insertComText; }
        }

        public TableParser(string tableName, DataTable dataTable)
        {
            _tableName = tableName;
            _dataTable = dataTable;
            Parse();
        }

        private void Parse()
        {
            var reader = _dataTable.CreateDataReader();
            _parameters = new NpgsqlParameter[reader.FieldCount - 1];
            List<string> list = new List<string>();

            for (int i = 1; i < reader.FieldCount; i++)
            {
                string fieldName = reader.GetName(i);
                Type fieldType = reader.GetFieldType(i);

                Console.WriteLine($"Введите поле {fieldName} (тип данных {fieldType.Name})");

                _parameters[i - 1] = new NpgsqlParameter($"@{fieldName}", Convert.ChangeType(Console.ReadLine(), fieldType));
                list.Add(fieldName);
            }

            string columns = string.Join(", ", list);
            string values = string.Join(", ", list.Select(item => $"@{item}"));

            _insertComText = $"INSERT INTO {_tableName} ({columns}) VALUES ({values})";
        }
    }
}
