﻿using Npgsql;
using System;
using System.Configuration;
using System.Data;

namespace DBLesson
{
    class CRUD
    {
        private string _connectionString;

        public CRUD()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["Npgsql"].ConnectionString;
        }

        public void Insert(string comText, NpgsqlParameter[] parameters)
        {
            using NpgsqlConnection connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            
            using NpgsqlCommand command = new NpgsqlCommand(comText, connection);
            command.Parameters.AddRange(parameters);
            
            command.ExecuteNonQuery();
        }

        public DataSet Select(string comText)
        {
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(comText, _connectionString);

            DataSet dataSet = new DataSet();
            adapter.Fill(dataSet);

            return dataSet;
        }

        public void Print(DataSet dataSet)
        {
            var reader = dataSet.CreateDataReader();
            do
            {
                //Печать шапки таблицы
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    Console.Write($"{reader.GetName(i),17}|");
                }
                Console.WriteLine();

                //Печать значений таблицы
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        Console.Write($"{reader[i],17}|");
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            } while (reader.NextResult());
        }
    }
}
