﻿using System;
using System.Collections.Generic;
using System.Data;

namespace DBLesson
{
    class Program
    {
        private static void Main(string[] args)
        {

            CRUD crud = new CRUD();
            TableParser tableParser;

            string comSelect = "SELECT * FROM clients;SELECT * FROM accounts;" +
                "SELECT * FROM operations;SELECT * FROM operations_type";
            List<string> list = new List<string>();
            list.AddRange(new string[] { "clients", "accounts", "operations", "opertions_type" });

            DataSet dataSet = crud.Select(comSelect);

            Console.WriteLine("Вывод данных:");
            crud.Print(dataSet);

            Console.WriteLine($"Выберите номер таблицы от 1 до {list.Count} в которую нужно вставить данные:\n{string.Join("\n",list)}");
            int.TryParse(Console.ReadLine(), out int num);

            try
            {
                tableParser = new TableParser(list[--num], dataSet.Tables[num]);
                crud.Insert(tableParser.InsertComText, tableParser.Parameters);
            }
            catch (Exception exc)
            {
                Console.WriteLine($"Некорректный ввод!{Environment.NewLine}{exc.Message}");
            }
        }
    }
}
